package neopackage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMywebapp3Application {

    public static void main(String[] args) {
        System.out.println("My Web Application 3");
        SpringApplication.run(DemoMywebapp3Application.class, args);
        System.out.println("Goodbye!");
    }

}
