package neopackage;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/helloworld")
    public String getHelloMessage() {
        return "Hello World!";
    }

    @GetMapping("/hellothere")
    public String getHello2Message() {
        return "Hello There!";
    }

    @GetMapping("/hello")
    public String getHello3Message() {
        return "Hello!";
    }

    @GetMapping("/hellowe")
    public String getHelloWeMessage() {
        return "Hello We!";
    }

    @GetMapping("/hellothem")
    public String getHelloThemMessage() {
        return "Hello Them!";
    }

    @GetMapping("/hellous")
    public String getHelloUsMessage() {
        return "Hello Us!";
    }

    @GetMapping("/hellofamily")
    public String getHelloFamilyMessage() {
        return "Hello Family!";
    }

}
